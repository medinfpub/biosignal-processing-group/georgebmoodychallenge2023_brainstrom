#!/usr/bin/env python

# Edit this script to add your team's code. Some functions are *required*, but you can edit most parts of the required functions,
# change or remove non-required functions, and add your own functions.

################################################################################
#
# Optional libraries, functions, and variables. You can change or remove them.
#
################################################################################
import fractions

from helper_code import *
import numpy as np, os
from scipy.signal import butter, lfilter, iirnotch, filtfilt, resample, resample_poly, decimate
from sklearn.preprocessing import RobustScaler, StandardScaler
import tensorflow as tf
from tensorflow.keras.layers import (Input, Conv1D, MaxPooling1D, Dropout, BatchNormalization, Activation, Add, Flatten, Dense)
from tensorflow.keras.models import Model, load_model
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.callbacks import (ModelCheckpoint, TensorBoard, ReduceLROnPlateau,
                                        CSVLogger, EarlyStopping)
from tensorflow.data import Dataset
from sklearn.model_selection import train_test_split
import math
from joblib import dump, load
from scipy.stats import kurtosis
import concurrent.futures
#import concurrent.futures

################################################################################
#
# Required functions. Edit these functions to add your code, but do not change the arguments of the functions.
#
################################################################################

scaler=None
batch_size = 512
parallel_jobs = 10

# Train your model.
def train_challenge_model(data_folder, model_folder, verbose):
    # Find data files.
    if verbose >= 1:
        print('Finding the Challenge data...')

    patient_ids = find_data_folders(data_folder)
    num_patients = len(patient_ids)

    

    if num_patients==0:
        raise FileNotFoundError('No data was provided.')

    # Create a folder for the model if it does not already exist.
    os.makedirs(model_folder, exist_ok=True)

    # Extract the features and labels.
    if verbose >= 1:
        print('Extracting features and labels from the Challenge data...')

    #Split Dataset into training and validation Dataset
    #training_ids, validation_ids = train_test_split(patient_ids, test_size=0.2, random_state=42)
    #print(validation_ids)
    training_ids = np.random.permutation(patient_ids)
    # Fit the robust scaler on the first 30 patients
   
    
    global scaler
    print('fitting scaler...')
    scaler = fit_scaler_on_patients(data_folder, patient_ids[:300])
    dump(scaler, os.path.join(model_folder, 'scaler.joblib'))
    
    train_dataset = Dataset.from_generator(lambda: get_data(verbose, data_folder, training_ids), output_signature=(tf.TensorSpec(shape=(4096, 18), dtype=tf.float32, name=None),
  tf.TensorSpec(shape=(), dtype=tf.float32, name=None)))
    #val_dataset = Dataset.from_generator(lambda: get_data(verbose, data_folder, validation_ids), output_signature=(tf.TensorSpec(shape=(4096, 18), dtype=tf.float32, name=None),tf.TensorSpec(shape=(), dtype=tf.float32, name=None)))
  

    
    #gen = get_data(verbose, data_folder, training_ids)
    #eeg_channels = ['Fp1-F7', 'F7-T3', 'T3-T5', 'T5-O1', 'Fp2-F8', 'F8-T4', 'T4-T6', 'T6-O2', 'Fp1-F3', 'F3-C3', 'C3-P3', 'P3-O1', 'Fp2-F4', 'F4-C4', 'C4-P4', 'P4-O2', 'Fz-Cz', 'Cz-Pz']

    #for X, _ in gen:
        #print(channel_equal(tf.transpose(X)))
        #plot_eeg([tf.transpose(X)], eeg_channels, 100, 10)

    train_dataset = train_dataset.batch(batch_size)
    #val_dataset = val_dataset.batch(batch_size)
    #print('calculating steps per epoch...')
    #steps_per_epoch = get_steps_per_epoch(training_ids, data_folder)


    # Optimization settings
    loss = 'binary_crossentropy'
    lr = 0.0001
    
    opt = Adam(lr)
    callbacks = [ReduceLROnPlateau(monitor='val_loss',
                                   factor=0.1,
                                   patience=7,
                                   min_lr=lr / 100),
                 EarlyStopping(patience=9,  # Patience should be larger than the one in ReduceLROnPlateau
                               min_delta=0.00001)]
    model = get_model(1)
    model.compile(loss=loss, optimizer=opt)

    
    # Create log

    callbacks += [TensorBoard(log_dir='./logs', write_graph=False),
                  CSVLogger('training.log', append=False)]  # Change append to true if continuing training
    # Save the BEST and LAST model
    
    callbacks += [ModelCheckpoint(f'{model_folder}/backup_model_last.hdf5'),
                  ModelCheckpoint(f'{model_folder}/backup_model_best.hdf5', save_best_only=True)]
    

    # Train neural network
    if verbose >= 1:
        print('Training the Challenge model on the Challenge data...')
    
    history = model.fit(train_dataset,
                        epochs = 4,
                        initial_epoch = 0,  # If you are continuing a interrupted section change here
                        callbacks = callbacks,
                        #validation_data = val_dataset,
                        #steps_per_epoch = steps_per_epoch,
                        verbose=1)

    # Save the models.
    save_challenge_model(model_folder, model)

    if verbose >= 1:
        print('Done.')

# Load your trained models. This function is *required*. You should edit this function to add your code, but do *not* change the
# arguments of this function.
def load_challenge_models(model_folder, verbose):
    filename = os.path.join(model_folder, 'model.hdf5')
    global scaler
    scaler = load(os.path.join(model_folder, 'scaler.joblib'))
    #5 epoch- loss: 0.6212 - val_loss: 0.6043 - lr: 0.0010

    return {'model': load_model(filename), 'scaler': scaler}

# Run your trained models. This function is *required*. You should edit this function to add your code, but do *not* change the
# arguments of this function.
def run_challenge_models(model, data_folder, patient_id, verbose):


    global scaler
    scaler = model['scaler']
    model = model['model']
    #train_dataset = Dataset.from_generator(lambda: get_data(verbose, data_folder, [patient_id], 'training'), output_signature=(tf.TensorSpec(shape=(2000, 19), dtype=tf.float32, name=None),tf.TensorSpec(shape=(), dtype=tf.float32, name=None)))
    data = get_data(verbose, data_folder, [patient_id])
    predictions = []
    entries = []
    for entry in data:
        outcome = entry[1]
        entry = entry[0]
        entries.append(entry)
    #print(np.shape(entries))
    try:
        entries = tf.expand_dims(entries, axis = 3)
    except:
        #if no eeg data was provided for this patient
        return 0, 0.444, 1
        
    predictions = model.predict(entries)
    
    predictions = [prediction[0] for prediction in predictions]
    print(patient_id, outcome.numpy(), predictions)   
    median = np.median(predictions)
    outcome_prob = median

    pred_outcome = 0 if median < 0.6 else 1
    if np.min(predictions) <= 0.1:
        pred_outcome = 0
        outcome_prob = np.min(predictions)
    elif np.max(predictions) >= 0.95:
        pred_outcome = 1
        outcome_prob = np.max(predictions)

    cpc = 1 if pred_outcome == 0 else 5
    print('pred_outcome: ', pred_outcome, ', actual_outcome: ', outcome.numpy(),', median: ', median)
    

    return pred_outcome, outcome_prob, cpc

################################################################################
#
# Optional functions. You can change or remove these functions and/or add new functions.
#
################################################################################

# Save your trained model.
def save_challenge_model(model_folder, model):
    filename = os.path.join(model_folder, 'model.hdf5')
    model.save(filename)

#Generator for partially load data
def channel_equal(data):
    return data[0][1000] == data[1][1000]
def get_data(verbose, data_folder, patient_ids):
    global scaler
    
    group = 'EEG'
    #split the data in training and validation data
    

    for patient_id in patient_ids:
        #print(patient_id)
        recording_ids = find_recording_files(data_folder, patient_id)
        # Extract labels.
        patient_metadata = load_challenge_data(data_folder, patient_id)
        try:
            current_outcome = get_outcome(patient_metadata)
        except:
            current_outcome = 0

        #take only later recordings above 50h
        #recording_ids = [i for i in recording_ids if int(i[-2:]) >= 20]
        

        if len(recording_ids) == 0:
            continue
        
        #parallel processing of the signals
        executor = concurrent.futures.ProcessPoolExecutor(parallel_jobs)
        futures = [executor.submit(get_data_parallel, (data_folder, patient_id, recording_id)) for recording_id in recording_ids] 
        
        concurrent.futures.wait(futures)
        
        for future in futures:
            try:
                result = future.result()
                for X in result:
                    X = scaler.transform(X.T)
                    X = filter_physiological_boundaries(X.T)
                    X = tf.transpose(X)
                    X = tf.cast(X, tf.float32)
                    Y = tf.cast(current_outcome, tf.float32)
                    #print(patient_id)
                    yield X, Y
            except:
                print('generator did not produce output')
        
        

def get_data_parallel(args):
    data_folder, patient_id, recording_id = args
   
    group = 'EEG'
    recording_location = os.path.join(data_folder, patient_id, '{}_{}'.format(recording_id, group))
    if not os.path.exists(recording_location + '.hea'):
        return []
    data, channels, sampling_frequency = load_recording_data(recording_location)
    data = reorder_recording_channels(data, channels)
    data = create_bipolar_montage(data)
    #recording_length = np.shape(data)[1]/int(sampling_frequency) 
    
    
    desired_sampling_frequency = 100
    
    downsample_factor = int(sampling_frequency/desired_sampling_frequency)
    data = decimate(data, downsample_factor)
    data = butter_bandpass_filter(data, lowcut=0.5, highcut=45, fs=desired_sampling_frequency)
    data = data[:, 1000:]
    #print(data[0][:10])
    #data = find_best_segment(data, step=10000)
    X = []
    batch_per_rec = 4
    step = 10000
    
    for i in range(batch_per_rec):
        if np.shape(data)[1] < (i+1)*step:
            break
        best_segment_start_idx, best_segment_end_idx = find_best_segment(data, step=step)
        #print(i, recording_id,best_segment_start_idx, best_segment_end_idx)
        
        X.append(data[:, best_segment_start_idx:best_segment_end_idx])
        if batch_per_rec > 1:
            data = np.concatenate((data[:,:best_segment_start_idx], np.zeros((18,4096)), data[:, best_segment_end_idx+1:]),axis=1)
    
   
    return X
def get_steps_per_epoch(patient_ids, data_folder):
    count=0
    
    for patient_id in patient_ids:

        recording_ids = find_recording_files(data_folder, patient_id)
        if len(recording_ids) == 0:
            continue
        executor = concurrent.futures.ProcessPoolExecutor(parallel_jobs)
        futures = [executor.submit(get_steps_per_epoch_parallel, (data_folder, patient_id, recording_id)) for recording_id in recording_ids] 
        
        concurrent.futures.wait(futures)
        count += sum([future.result() for future in futures])

                
    return math.ceil(count / batch_size)
def get_steps_per_epoch_parallel(args):
    data_folder, patient_id, recording_id = args
    group = 'EEG'
    recording_location = os.path.join(data_folder, patient_id, '{}_{}'.format(recording_id, group))
    if not os.path.exists(recording_location + '.hea'):
        return 0
    data, _, sampling_frequency = load_recording_data(recording_location)

    recording_length = np.shape(data)[1]/int(sampling_frequency) 
    if recording_length < 60:
        return 0
    return 1

def fit_scaler_on_patients(data_folder, patient_ids):

    executor = concurrent.futures.ProcessPoolExecutor(parallel_jobs)
    futures = [executor.submit(get_eeg, (data_folder, patient_id)) for patient_id in patient_ids] 
    concurrent.futures.wait(futures)

    all_data = [future.result() for future in futures]    
    all_data = np.concatenate(all_data, axis=1)  # concatenate along the second dimension

    scaler = RobustScaler().fit(all_data.T)
    return scaler
""" def plot_eeg(eeg_data_list, channel_names, fs=10, duration=30, num_instances=5):
    num_samples = fs * duration
    for i, eeg_data in enumerate(eeg_data_list[:num_instances]):
        fig, axs = plt.subplots(len(channel_names), figsize=(15, 15))
        fig.suptitle(f'EEG instance {i + 1}')
        for j, channel in enumerate(channel_names):
            axs[j].plot(eeg_data[j, :num_samples])
            axs[j].set_title(f'Channel {channel}')
        plt.subplots_adjust(hspace=0.5, top=0.9)
        plt.show() """

def get_eeg(args):
    data_folder, patient_id = args
    if isinstance(data_folder, bytes):
        data_folder = data_folder.decode()


    if isinstance(patient_id, bytes):
        patient_id = patient_id.decode()
    group = 'EEG'
    if isinstance(group, bytes):
        group = group.decode()

    current_eegs = list()
    eegs_time = list()
    # Load patient data.
    recording_ids = find_recording_files(data_folder, patient_id)
    num_recordings = len(recording_ids)
    last_eeg_full_hour = np.zeros((18, 60*60*100))
    #gets the last eeg available with length of 60min (=eeg)
    if num_recordings > 0:
        recording_id = recording_ids[-1]
        recording_location = os.path.join(data_folder, patient_id, '{}_{}'.format(recording_id, group))
        if os.path.exists(recording_location + '.hea'):
            data, channels, sampling_frequency = load_recording_data(recording_location)
            recording_length = np.shape(data)[1]/int(sampling_frequency)/60
            if not recording_length == 60: #only keep recordings of 1 hour length
                recording_ids = sorted(list(set(recording_ids))) # removes double values (through ecg and other signals)
                num_eeg_recordings = len(recording_ids)
                for recording in range(num_eeg_recordings):
                    recording_id = recording_ids[-(recording+1)]
                    recording_location = os.path.join(data_folder, patient_id, '{}_{}'.format(recording_id, group))
                    if os.path.exists(recording_location + '.hea'):
                        data, channels, sampling_frequency = load_recording_data(recording_location)
                        recording_length = np.shape(data)[1] / int(sampling_frequency) / 60
                        if recording_length == 60:
                            break
            data = reorder_recording_channels(data, channels)            
            data = create_bipolar_montage(data)
            data = butter_bandpass_filter(data, lowcut=0.5, highcut=45, fs=sampling_frequency)            
            last_eeg_full_hour = preprocess_eeg(data, recording_length=recording_length)

    return last_eeg_full_hour
def search_index(data, window=4096):
    for i in range(len(data[0])-window):
        if not np.isnan(data[0][i]):
            return i
    return 0
         
def create_bipolar_montage(data, channels=['Fp1', 'F7', 'T3', 'T5', 'O1', 'Fp2', 'F8', 'T4', 'T6', 'O2', 'F3', 'C3', 'P3', 'F4', 'C4', 'P4',
                    'Fz', 'Cz', 'Pz'], pairs=[('Fp1', 'F7'), ('F7', 'T3'), ('T3', 'T5'), ('T5', 'O1'), ('Fp2', 'F8'), ('F8', 'T4'), ('T4', 'T6'), ('T6', 'O2'), ('Fp1', 'F3'), ('F3', 'C3'), ('C3', 'P3'), ('P3', 'O1'), ('Fp2', 'F4'), ('F4', 'C4'), ('C4', 'P4'), ('P4', 'O2'), ('Fz', 'Cz'), ('Cz', 'Pz')]):
    '''
    data: raw eeg data with shape (channels, timepoints)
    channels: list of channels in the same order as they appear in the data
    pairs: list of pairs of channel names
    '''
    # Initialize a new array to store the bipolar data
    bipolar_data = np.zeros((len(pairs), data.shape[1]))

    # Subtract each pair of channels
    for pair_idx, pair in enumerate(pairs):
        ch1_idx = channels.index(pair[0])
        ch2_idx = channels.index(pair[1])
        bipolar_data[pair_idx, :] = data[ch1_idx, :] - data[ch2_idx, :]
    return bipolar_data
def filter_physiological_boundaries(data, threshold=2.5):
    """Exclude values outside of physiological boundaries."""
    mask = np.abs(data) <= threshold  # Create a mask of values within the threshold
    data[~mask] = 0  # Set values outside of the threshold to zero
    return data
# Define Butterworth bandpass filter
def butter_bandpass(lowcut, highcut, fs, order=5):
    nyq = 0.5 * fs
    low = lowcut / nyq
    high = highcut / nyq
    b, a = butter(order, [low, high], btype='band')
    return b, a


def butter_bandpass_filter(data, lowcut, highcut, fs, order=5):
    b, a = butter_bandpass(lowcut, highcut, fs, order=order)
    if len(data.shape) == 1:  # If data is 1D, just apply the filter
        y = lfilter(b, a, data)
    elif len(data.shape) == 2:  # If data is 2D, apply the filter to each channel
        y = np.empty_like(data)  # Create an empty array with the same shape as data
        for channel in range(data.shape[0]):
            y[channel, :] = lfilter(b, a, data[channel, :])
    else:
        raise ValueError('Input data has an unsupported shape.')
    return y


def find_best_segment(eeg_data, segment_timepoints=4096, step=1):
    """
    Finds a 40-second segment of EEG data with median variance and the lowest kurtosis.

    Parameters:
    - eeg_data: A 2D numpy array with shape (num_channels, num_timepoints)
    - sample_frequency: Sampling frequency of the EEG data, default is 100 Hz
    - segment_duration: Duration of the segment to extract, default is 40 seconds
    - step: The step size for the moving window, default is 1

    Returns:
    - best_segment: A 2D numpy array of the best segment
    """
  
    
    # Check if the EEG data is long enough for the segment duration
    if eeg_data.shape[1] < segment_timepoints:
        raise ValueError(f"EEG data is too short.")

    # Initialize variables to store best segment's stats and indices
    best_kurtosis = float('inf')
    best_segment_start_idx = 0
    best_segment_end_idx = segment_timepoints
    median_variance = np.median(np.var(eeg_data, axis=1))
    best_diff = float('inf')

    # Loop through the EEG data with a moving window
    for start_idx in range(0, eeg_data.shape[1] - segment_timepoints + 1, step):
       
        end_idx = start_idx + segment_timepoints
        
        # Extract segment
        segment = eeg_data[:, start_idx:end_idx]
        
        # Calculate variance and kurtosis
        var_values = np.var(segment, axis=1)
        segment_kurtosis = kurtosis(segment, axis=1).sum()

        # Check if variance is close to the median variance of the EEG data
        
        diff = np.abs(np.median(var_values) - median_variance)
        

        if diff < best_diff and segment_kurtosis < best_kurtosis:
            best_diff = diff
            best_kurtosis = segment_kurtosis
            best_segment_start_idx = start_idx
            best_segment_end_idx = end_idx

    # Extract and return the best segment
    
    #best_segment = eeg_data[:, best_segment_start_idx:best_segment_end_idx]
    return best_segment_start_idx, best_segment_end_idx



# Define notch filter
def apply_notch(data, freq, fs, q=30):
    freq = np.array(freq)
    for f in freq:
        b, a = iirnotch(f, q, fs)
        data = filtfilt(b, a, data)
    return data

#Add notch frequency if highcut over 50/60
def preprocess_eeg(eeg_data, recording_length=60):
    # Sampling_rate = 100
    # get the original number of timepoints
    original_timepoints = eeg_data.shape[1]
    # get the desired number of timepoints
    desired_timepoints = int(recording_length*60*100)
    # find a fraction that closely represents the desired ratio
    frac = fractions.Fraction(desired_timepoints / original_timepoints).limit_denominator()
    # approximately resample data
    resampled_data_approx = resample_poly(eeg_data, frac.numerator, frac.denominator, axis=1)
    # finely adjust the number of samples
    eeg = resample(resampled_data_approx, desired_timepoints, axis=1)
    #eeg = eeg[:, :10000]
    # Apply bandpass filter
    return eeg

def reorder_recording_channels(current_data, current_channels):
    # Order EEG channels; excluded F9, Fpz, Oz since there are not present in every recording

    reordered_channels = ['Fp1', 'F7', 'T3', 'T5', 'O1', 'Fp2', 'F8', 'T4', 'T6', 'O2', 'F3', 'C3', 'P3', 'F4', 'C4',
                    'P4', 'Fz', 'Cz', 'Pz']
    if current_channels == reordered_channels:
        return current_data
    else:
        indices = list()
        for channel in reordered_channels:
            if channel in current_channels:
                i = current_channels.index(channel)
                indices.append(i)
        num_channels = len(reordered_channels)
        num_samples = np.shape(current_data)[1]
        reordered_data = np.zeros((num_channels, num_samples))
        reordered_data[:, :] = current_data[indices, :]
        return reordered_data


def compute_loss(ages, pred_ages, weights):
    diff = ages.flatten() - pred_ages.flatten()
    loss = nn.sum(weights.flatten() * diff * diff)
    return loss


class ResidualUnit(object):
    """Residual unit block (unidimensional).
    Parameters
    ----------
    n_samples_out: int
        Number of output samples.
    n_filters_out: int
        Number of output filters.
    kernel_initializer: str, optional
        Initializer for the weights matrices. See Keras initializers. By default it uses
        'he_normal'.
    dropout_keep_prob: float [0, 1), optional
        Dropout rate used in all Dropout layers. Default is 0.8
    kernel_size: int, optional
        Kernel size for convolutional layers. Default is 17.
    preactivation: bool, optional
        When preactivation is true use full preactivation architecture proposed
        in [1]. Otherwise, use architecture proposed in the original ResNet
        paper [2]. By default it is true.
    postactivation_bn: bool, optional
        Defines if you use batch normalization before or after the activation layer (there
        seems to be some advantages in some cases:
        https://github.com/ducha-aiki/caffenet-benchmark/blob/master/batchnorm.md).
        If true, the batch normalization is used before the activation
        function, otherwise the activation comes first, as it is usually done.
        By default it is false.
    activation_function: string, optional
        Keras activation function to be used. By default 'relu'.
    References
    ----------
    .. [1] K. He, X. Zhang, S. Ren, and J. Sun, "Identity Mappings in Deep Residual Networks,"
           arXiv:1603.05027 [cs], Mar. 2016. https://arxiv.org/pdf/1603.05027.pdf.
    .. [2] K. He, X. Zhang, S. Ren, and J. Sun, "Deep Residual Learning for Image Recognition," in 2016 IEEE Conference
           on Computer Vision and Pattern Recognition (CVPR), 2016, pp. 770-778. https://arxiv.org/pdf/1512.03385.pdf
    """

    def __init__(self, n_samples_out, n_filters_out, kernel_initializer='he_normal',
                 dropout_keep_prob=0.8, kernel_size=17, preactivation=True,
                 postactivation_bn=False, activation_function='relu'):
        self.n_samples_out = n_samples_out
        self.n_filters_out = n_filters_out
        self.kernel_initializer = kernel_initializer
        self.dropout_rate = 1 - dropout_keep_prob
        self.kernel_size = kernel_size
        self.preactivation = preactivation
        self.postactivation_bn = postactivation_bn
        self.activation_function = activation_function

    def _skip_connection(self, y, downsample, n_filters_in):
        """Implement skip connection."""
        # Deal with downsampling
        if downsample > 1:
            y = MaxPooling1D(downsample, strides=downsample, padding='same')(y)
        elif downsample == 1:
            y = y
        else:
            raise ValueError("Number of samples should always decrease.")
        # Deal with n_filters dimension increase
        if n_filters_in != self.n_filters_out:
            # This is one of the two alternatives presented in ResNet paper
            # Other option is to just fill the matrix with zeros.
            y = Conv1D(self.n_filters_out, 1, padding='same',
                       use_bias=False, kernel_initializer=self.kernel_initializer)(y)
        return y

    def _batch_norm_plus_activation(self, x):
        if self.postactivation_bn:
            x = Activation(self.activation_function)(x)
            x = BatchNormalization(center=False, scale=False)(x)
        else:
            x = BatchNormalization()(x)
            x = Activation(self.activation_function)(x)
        return x

    def __call__(self, inputs):
        """Residual unit."""
        x, y = inputs
        n_samples_in = y.shape[1]
        downsample = n_samples_in // self.n_samples_out
        n_filters_in = y.shape[2]
        y = self._skip_connection(y, downsample, n_filters_in)
        # 1st layer
        x = Conv1D(self.n_filters_out, self.kernel_size, padding='same',
                   use_bias=False, kernel_initializer=self.kernel_initializer)(x)
        x = self._batch_norm_plus_activation(x)
        if self.dropout_rate > 0:
            x = Dropout(self.dropout_rate)(x)

        # 2nd layer
        x = Conv1D(self.n_filters_out, self.kernel_size, strides=downsample,
                   padding='same', use_bias=False,
                   kernel_initializer=self.kernel_initializer)(x)
        if self.preactivation:
            x = Add()([x, y])  # Sum skip connection and main connection
            y = x
            x = self._batch_norm_plus_activation(x)
            if self.dropout_rate > 0:
                x = Dropout(self.dropout_rate)(x)
        else:
            x = BatchNormalization()(x)
            x = Add()([x, y])  # Sum skip connection and main connection
            x = Activation(self.activation_function)(x)
            if self.dropout_rate > 0:
                x = Dropout(self.dropout_rate)(x)
            y = x
        return [x, y]


def get_model(n_classes, last_layer='sigmoid'):
    kernel_size = 16
    kernel_initializer = 'he_normal'
    signal = Input(shape=(4096, 18), dtype=np.float32, name='signal')
    
    x = signal
    x = Conv1D(64, kernel_size, padding='same', use_bias=False,
               kernel_initializer=kernel_initializer)(x)
    x = BatchNormalization()(x)
    x = Activation('relu')(x)
    x, y = ResidualUnit(1024, 128, kernel_size=kernel_size,
                        kernel_initializer=kernel_initializer)([x, x])
    x, y = ResidualUnit(256, 196, kernel_size=kernel_size,
                        kernel_initializer=kernel_initializer)([x, y])
    x, y = ResidualUnit(64, 256, kernel_size=kernel_size,
                        kernel_initializer=kernel_initializer)([x, y])
    x, _ = ResidualUnit(16, 320, kernel_size=kernel_size,
                        kernel_initializer=kernel_initializer)([x, y])
    x = Flatten()(x)
    diagn = Dense(n_classes, activation=last_layer, kernel_initializer=kernel_initializer)(x)
    model = Model(signal, diagn)
    return model

