# GeorgeBMoodyChallenge2023_BrAInstrom

This repo contains the code contributed to the 2023 George B. Moody Challenge by the team BrAInstorm. 

The developed code consists of 2 approaches:
- Feature extraction techniques for EEG and ECG, absolute and relative power band, coherence & coupling for EEG and deceleration capacity for ECG (available at team_code_feature_extraction_BrAInstorm.py)
- End-to-end classifier for EEG, adaption of existing open source neural network for 12 lead ECG classification to standard 19 channel EEG recordings (available at team_code_resnet._BrAInstorm.py). The original open source neural network can be found at: https://github.com/antonior92/automatic-ecg-diagnosis

Additionally, functions to adapt the bipolar montage and plot EEGs can be found in bipolar_rereferencing.py
