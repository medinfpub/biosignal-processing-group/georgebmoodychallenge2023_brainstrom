"""
This script contains possibilites to adjust the bipolar referencing developed by the Team BrAInstorm for the 2023 George B. Moody Challenge;
There are dictionaries to use as input for the functions 'create_bipolar_monatage' and 'plot_EEG' 

This can be used for re-referencing EEG channels by the pairs which is a classic denoise technique when dealing with EEG data
"""

#dictionaries consisting of pairs and names that can be inserted in the create_bipolar_montage() function; pairs are substracted within the function and names are only for the plotting function to display the correct re-referenced channels in the EEG
sagittal_pairs = [('Fp1', 'F7'), ('F7', 'T3'), ('T3', 'T5'), ('T5', 'O1'), ('Fp2', 'F8'), ('F8', 'T4'), ('T4', 'T6'), ('T6', 'O2'), ('Fp1', 'F3'), ('F3', 'C3'), ('C3', 'P3'), ('P3', 'O1'), ('Fp2', 'F4'), ('F4', 'C4'), ('C4', 'P4'), ('P4', 'O2'), ('Fz', 'Cz'), ('Cz', 'Pz')]
sagittal_coronal_pairs = [('Fp1', 'F7'), ('F7', 'T3'), ('T3', 'T5'), ('T5', 'O1'), ('Fp2', 'F8'), ('F8', 'T4'), ('T4', 'T6'), ('T6', 'O2'), ('Fz', 'Cz'), ('Cz', 'Pz'), ('Fp1', 'Fp2'), ('F7', 'F8'), ('T3', 'T4'), ('C3', 'C4'), ('T5', 'T6'), ('P3', 'P4'), ('O1', 'O2')]
regional_pairs = [('Fp1', 'F7'), ('F7', 'F3'), ('F3', 'Fz'), ('Fz', 'F4'), ('F4', 'F8'), ('F8', 'Fp2'),('T3', 'T5'), ('T5', 'T6'), ('T6', 'T4'), ('C3', 'Cz'), ('Cz', 'C4'), ('P3', 'Pz'), ('Pz', 'P4'), ('O1', 'O2')]
frontal_pairs = [('Fp1', 'F7'), ('F7', 'F3'), ('F3', 'Fz'), ('Fz', 'F4'), ('F4', 'F8'), ('F8', 'Fp2')]
temporal_pairs = [('T3', 'T5'), ('T5', 'T6'), ('T6', 'T4')]
central_pairs = [('C3', 'Cz'), ('Cz', 'C4')]
parietal_pairs = [('P3', 'Pz'), ('Pz', 'P4')]
occipital_pairs = [('O1', 'O2')]

frontal_channels = ['Fp1', 'Fp2', 'F7', 'F8', 'F3', 'F4', 'Fz']
temporal_channels = ['T3', 'T4', 'T5', 'T6']
central_channels = ['C3', 'C4', 'Cz']
parietal_channels = ['P3', 'P4', 'Pz']
occipital_channels = ['O1', 'O2']


def create_bipolar_montage(data, channels=['Fp1', 'F7', 'T3', 'T5', 'O1', 'Fp2', 'F8', 'T4', 'T6', 'O2', 'F3', 'C3', 'P3', 'F4', 'C4', 'P4',
                    'Fz', 'Cz', 'Pz'], pairs=[('Fp1', 'F7'), ('F7', 'T3'), ('T3', 'T5'), ('T5', 'O1'), ('Fp2', 'F8'), ('F8', 'T4'), ('T4', 'T6'), ('T6', 'O2'), ('Fp1', 'F3'), ('F3', 'C3'), ('C3', 'P3'), ('P3', 'O1'), ('Fp2', 'F4'), ('F4', 'C4'), ('C4', 'P4'), ('P4', 'O2'), ('Fz', 'Cz'), ('Cz', 'Pz')]):
    '''
    data: raw eeg data with shape (channels, timepoints)
    channels: list of channels in the same order as they appear in the data
    pairs: list of pairs of channel names
    '''
    # Initialize a new array to store the bipolar data
    bipolar_data = np.zeros((len(pairs), data.shape[1]))

    # Subtract each pair of channels
    for pair_idx, pair in enumerate(pairs):
        ch1_idx = channels.index(pair[0])
        ch2_idx = channels.index(pair[1])
        bipolar_data[pair_idx, :] = data[ch1_idx, :] - data[ch2_idx, :]
    return bipolar_data


# names for the plot_function
frontal_names = ['Fp1-F7', 'F7-F3', 'F3-Fz', 'Fz-F4', 'F4-F8', 'F8-Fp2']
temporal_names = ['T3-T5', 'T5-T6', 'T6-T4']
central_names = ['C3-Cz', 'Cz-C4']
parietal_names = ['P3-Pz', 'Pz-P4']
occipital_names = ['O1-O2']

def plot_eeg(eeg_data, channel_names, fs=100, duration=30, title='name_me', save_path='name_me', outcome='NA',
             CPC='NA'):
    assert isinstance(eeg_data, (np.ndarray, list)), "eeg_data should be a numpy array or list"
    assert len(eeg_data) == len(
        channel_names), "The number of channels in eeg_data should match the number of channel_names"

    if isinstance(eeg_data, list):
        assert isinstance(eeg_data[0], list), "If eeg_data is a list, it should be a list of lists"
    elif isinstance(eeg_data, np.ndarray):
        assert len(eeg_data.shape) == 2, "If eeg_data is a numpy array, it should be two-dimensional"

    num_samples = fs * duration
    time_vector = np.arange(num_samples) / fs  # create a time vector in seconds
    fig, axs = plt.subplots(len(channel_names), figsize=(20, 20))
    fig.suptitle(f'{title}_Outcome:{outcome}_CPC:{CPC}')

    for j, channel in enumerate(channel_names):
        axs[j].plot(time_vector, eeg_data[j, :num_samples])  # pass time_vector as the first argument to plot
        axs[j].set_title(f'Channel {channel}')
        if j == len(channel_names) - 1:  # if it's the last subplot
            axs[j].set_xlabel('Time (seconds)')  # label the x-axis as Time (seconds)

    plt.subplots_adjust(hspace=0.8, top=0.95) # Adjust the spaces within the plot here (according to your channel need)
    plt.savefig(f'{save_path}.pdf')
    plt.clf() #clear plt to make it possible to do this within the patien loop of the challenge infrastructure



