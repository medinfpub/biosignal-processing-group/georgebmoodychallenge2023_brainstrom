#!/usr/bin/env python

# Edit this script to add your team's code. Some functions are *required*, but you can edit most parts of the required functions,
# change or remove non-required functions, and add your own functions.

################################################################################
#
# Optional libraries, functions, and variables. You can change or remove them.
#
################################################################################
from helper_code import *
import numpy as np, os, sys, json, fractions, tensorflow as tf, pickle, glob, math, concurrent.futures, queue, re, gc, keras

from tensorflow.keras.utils import Sequence
from scipy.signal import butter, lfilter, resample_poly, resample, welch, coherence, hilbert
from scipy.integrate import simps
from sklearn.preprocessing import RobustScaler
from joblib import dump, load
from collections import defaultdict
from tensorflow.keras.metrics import AUC
from tensorflow.keras import layers
import neurokit2 as nk
from keras.utils import to_categorical

################################################################################
#
# Required functions. Edit these functions to add your code, but do not change the arguments of the functions.
#
################################################################################
# Train your model.
def train_challenge_model(data_folder, model_folder, verbose):
    # Find data files.
    if verbose >= 1:
        print('Finding the Challenge data...')

    patient_ids = find_data_folders(data_folder)
    num_patients = len(patient_ids)

    if num_patients==0:
        raise FileNotFoundError('No data was provided.')

    # Create a folder for the model if it does not already exist.
    os.makedirs(model_folder, exist_ok=True)

    # Extract the features and labels.
    if verbose >= 1:
        print('Extracting features and labels from the Challenge data...')
    categorical = list()
    numerical = list()
    timeseries = list()
    outcomes = list()
    cpcs = list()


    # Initialize an empty dictionary to hold your models and scalers
    models_and_scalers = {'models': [], 'scalers': {}}
    scaler_bipolar, scaler_raw = fit_scaler_on_patients(data_folder, patient_ids[:300])
    models_and_scalers['scalers']['raw_scaler'] = scaler_raw
    models_and_scalers['scalers']['bipolar_scaler'] = scaler_bipolar

    for i in range(num_patients):
        if verbose >= 2:
            print('    {}/{}...'.format(i + 1, num_patients))
        patient_id = patient_ids[i]
        print(patient_id)
        # Get Timeseries
        current_timeseries = get_timeseries(data_folder, patient_id, scaler_bipolar, scaler_raw)
        print(np.shape(current_timeseries))
        timeseries.append(current_timeseries)
        # Extract categorical data.
        patient_metadata = load_challenge_data(data_folder, patient_id)
        current_categorical = get_categorical(patient_metadata)
        categorical.append(current_categorical)

        # Extract numerical data.
        current_numerical = get_numerical(data_folder, patient_id)
        numerical.append(current_numerical)

        # Extract labels.
        current_outcome = get_outcome(patient_metadata)
        outcomes.append(current_outcome)
        current_cpc = get_cpc(patient_metadata)
        cpcs.append(current_cpc)

    X_train_timeseries = np.array(timeseries)
    print(np.shape(timeseries))
    outcomes = np.vstack(outcomes)
    cpcs = np.vstack(cpcs)
    X_train_categorical = np.array(categorical)
    X_train_numerical = np.array(numerical)

    scalers = {}
    num_train_samples, n_timepoints, n_channels, n_freq_bands, n_features = X_train_timeseries.shape

    for feature in range(n_features):  # 2 features (e.g., relative and absolute power bands)
        scalers[feature] = RobustScaler()
        data = X_train_timeseries[..., feature].reshape(-1, 1)
        X_train_timeseries[..., feature] = scalers[feature].fit_transform(data).reshape(num_train_samples, n_timepoints,
                                                                                  n_channels, n_freq_bands)

        models_and_scalers['scalers'][f'scaler_{feature}'] = scalers[feature]
    X_train_timeseries = X_train_timeseries.reshape(
        num_train_samples, n_timepoints, n_channels * n_freq_bands * n_features
    )

    train_generator = DataGenerator(X_train_categorical, X_train_numerical, X_train_timeseries, outcomes, batch_size=16)

    outcome = create_model(18,7, (876, 360), output_size=True)
    outcome = outcome.build()
    outcome.fit(train_generator, epochs=6)
    models_and_scalers['models'].append(outcome)

    cpcs_onehot = to_categorical(cpcs - 1, num_classes=5)
    train_generator = DataGenerator(X_train_categorical, X_train_numerical, X_train_timeseries, cpcs_onehot, batch_size=16)
    cpc = create_model(18, 7, (876, 360), output_size=False)
    cpc = cpc.build()
    cpc.fit(train_generator, epochs=12)

    models_and_scalers['models'].append(cpc)

     #Save the models.
    save_challenge_model(model_folder, models_and_scalers)

    if verbose >= 1:
        print('Done.')

# Load your trained models. This function is *required*. You should edit this function to add your code, but do *not* change the
# arguments of this function.
def load_challenge_models(model_folder, verbose):
    model_files = glob.glob(os.path.join(model_folder, "model_*.keras"))
    models = [tf.keras.models.load_model(model_file) for model_file in model_files]

    scaler_path = os.path.join(model_folder, 'scalers.pkl')
    with open(scaler_path, 'rb') as f:
        scalers = pickle.load(f)

    return {"models": models, "scalers": scalers}

# Run your trained models. This function is *required*. You should edit this function to add your code, but do *not* change the
# arguments of this function.
def run_challenge_models(models, data_folder, patient_id, verbose):
    outcome_model = models['models'][0]
    cpc_model = models['models'][1]

    patient_metadata = load_challenge_data(data_folder, patient_id)
    categorical = get_categorical(patient_metadata)
    numerical = get_numerical(data_folder, patient_id)
    timeseries = get_timeseries(data_folder, patient_id, models['scalers']['bipolar_scaler'], models['scalers']['raw_scaler'])
    print(np.shape(timeseries))

    n_timepoints, n_channels, n_freq_bands, n_features = timeseries.shape
    for feature in range(n_features):
        data_feature = timeseries[..., feature].reshape(-1, 1)
        timeseries[..., feature] = models['scalers'][f'scaler_{feature}'].transform(data_feature).reshape(n_timepoints,
                                                                                     n_channels, n_freq_bands)
    timeseries = timeseries.reshape(
        n_timepoints, n_channels * n_freq_bands * n_features
    )
    X_val_categorical = tf.expand_dims(categorical, axis=0)
    X_val_numerical = tf.expand_dims(numerical, axis=0)
    X_val_timeseries = tf.expand_dims(timeseries, axis=0)

    outcome_prediction = outcome_model.predict({
        "input_categorical": X_val_categorical,
        "input_numerical": X_val_numerical,
        "input_timeseries": X_val_timeseries
    })

    outcome = np.round(outcome_prediction)[0][0]
    outcome_probability = outcome_prediction[0][0]

    cpc_prediction = cpc_model.predict({
        "input_categorical": X_val_categorical,
        "input_numerical": X_val_numerical,
        "input_timeseries": X_val_timeseries
    })
    cpc = np.argmax(cpc_prediction, axis=1) + 1
    print(cpc)
    if verbose >= 1:
        print(outcome, outcome_probability, cpc[0])
    return outcome, outcome_probability, cpc[0]
################################################################################
#
# Optional functions. You can change or remove these functions and/or add new functions.
#
################################################################################

# Save your trained model.
def save_challenge_model(model_folder, models_and_scalers):
    if not os.path.exists(model_folder):
        os.makedirs(model_folder)

    # Save the models
    for i, model in enumerate(models_and_scalers["models"]):
        model_path = os.path.join(model_folder, f'model_{i}.keras')
        model.save(model_path)

    # Save the scalers using pickle
    scaler_path = os.path.join(model_folder, 'scalers.pkl')
    with open(scaler_path, 'wb') as f:
        pickle.dump(models_and_scalers["scalers"], f)

def safe_replace_nan_with_zero(data):
    if isinstance(data, np.ndarray):
        data[np.isnan(data)] = 0
    else:
        if np.isnan(data):
            data = 0.0
    return data

def process_recording(data_folder, group, recording_id):
    recording_location = os.path.join(data_folder, str(recording_id)[0:4], '{}_{}'.format(recording_id, group))
    if os.path.exists(recording_location + '.hea'):
        current_time = int(recording_location[-7:-4])
        data, channels, sampling_frequency = load_recording_data(recording_location)
        data = filter_physiological_boundaries(data)
        data = reorder_recording_channels(data, channels)
        data = butter_bandpass_filter(data, lowcut=0.5, highcut=45, fs=sampling_frequency)
        return data, current_time, sampling_frequency
    return None, None, None

def compute_metrics(data, sampling_frequency, scaler_bipolar, scaler_raw):
    recording_length = data.shape[1] / int(sampling_frequency) / 60
    data_bipolar = create_bipolar_montage(data)
    current_eeg = preprocess_eeg(data_bipolar, recording_length=recording_length)
    current_eeg = scaler_bipolar.transform(current_eeg.T)
    current_eeg = np.array(current_eeg.T)
    current_raw = preprocess_eeg(data, recording_length=recording_length)
    current_raw = scaler_raw.transform(current_raw.T)
    current_raw = np.array(current_raw.T)

    if np.shape(current_eeg)[1] > 360000:
        current_eeg = current_eeg[:, :360000]
        current_raw = current_raw[:, :360000]
    powerbands = calculate_all_bandpowers(current_eeg, fs=100)
    pair_coherence_values = calculate_all_coherences(current_raw)
    pair_coherence_values = np.expand_dims(pair_coherence_values, axis=-1)
    pac_values_all = calculate_window_pac(current_raw)
    pac_values_all = np.expand_dims(pac_values_all, axis=-1)
    results = np.concatenate((powerbands, pair_coherence_values, pac_values_all), axis=-1)

    return results

def parallel_compute_metrics(data_folder, group, recording_id, scaler_bipolar, scaler_raw):
    try:
        data, current_time, sampling_frequency = process_recording(data_folder, group, recording_id)
        if data is None:
            return None, None
        result = compute_metrics(data, sampling_frequency, scaler_bipolar, scaler_raw)
        return result, current_time
    except Exception as e:
        print(f"Error in parallel_compute_metrics for recording {recording_id}: {e}")
        return None, None

def get_timeseries(data_folder, patient_id, scaler_bipolar, scaler_raw):
    try:
        recording_ids = find_recording_files(data_folder, patient_id)
        current_psd = None

        # Use parallel processing for computing metrics
        with concurrent.futures.ProcessPoolExecutor(16) as executor:
            results = list(executor.map(parallel_compute_metrics,
                                        [data_folder] * len(recording_ids),
                                        ['EEG'] * len(recording_ids),
                                        recording_ids,
                                        [scaler_bipolar] * len(recording_ids),
                                        [scaler_raw] * len(recording_ids)))

        # Process results to get the results in the desired format
        results = sorted([r for r in results if r[0] is not None], key=lambda x: x[1])

        for result, current_time in results:
            if result.shape[0] > 0:
                if current_psd is None:
                    current_psd = np.zeros((73 * 12,) + result.shape[1:])
                start = (current_time) * 12
                end = start + result.shape[0]
                current_psd[start:end, :] = result

        print(current_psd)

        if current_psd is None:
            current_psd = np.zeros((876, 18, 5, 4))
    except Exception as Error:
        print(Error)
        current_psd = np.zeros((876, 18, 5, 4))

    return current_psd




def get_last_eeg(data_folder, patient_id, raw=False):
    recording_ids = find_recording_files(data_folder, patient_id)
    num_recordings = len(recording_ids)
    group = 'EEG'

    #gets the last eeg available with length of 60min (=eeg)
    if num_recordings > 0:
        recording_id = recording_ids[-1]
        recording_location = os.path.join(data_folder, patient_id, '{}_{}'.format(recording_id, group))
        if os.path.exists(recording_location + '.hea'):
            data, channels, sampling_frequency = load_recording_data(recording_location)
            recording_length = np.shape(data)[1]/int(sampling_frequency)/60
            if not recording_length == 60: #only keep recordings of 1 hour length
                recording_ids = sorted(list(set(recording_ids))) # removes double values (through ecg and other signals)
                num_eeg_recordings = len(recording_ids)
                for recording in range(num_eeg_recordings):
                    recording_id = recording_ids[-(recording+1)]
                    recording_location = os.path.join(data_folder, patient_id, '{}_{}'.format(recording_id, group))
                    if os.path.exists(recording_location + '.hea'):
                        data, channels, sampling_frequency = load_recording_data(recording_location)
                        recording_length = np.shape(data)[1] / int(sampling_frequency) / 60
                        if recording_length == 60:
                            break
            data = filter_physiological_boundaries(data)
            data = butter_bandpass_filter(data, lowcut=0.5, highcut=45, fs=sampling_frequency)
            data = reorder_recording_channels(data, channels)
            if raw==True:
                pass
            else:
                data = create_bipolar_montage(data)
            data = preprocess_eeg(data, recording_length=recording_length)
    else:
        data = np.empty(shape=(0, 0))  # Return an empty numpy array when there's no valid recording
    return data #


def reorder_recording_channels(current_data, current_channels):
    # Order EEG channels; excluded F9, Fpz, Oz since there are not present in every recording

    reordered_channels = ['Fp1', 'F7', 'T3', 'T5', 'O1', 'Fp2', 'F8', 'T4', 'T6', 'O2', 'F3', 'C3', 'P3', 'F4', 'C4',
                    'P4', 'Fz', 'Cz', 'Pz']
    if current_channels == reordered_channels:
        return current_data
    else:
        indices = list()
        for channel in reordered_channels:
            if channel in current_channels:
                i = current_channels.index(channel)
                indices.append(i)
        num_channels = len(reordered_channels)
        num_samples = np.shape(current_data)[1]
        reordered_data = np.zeros((num_channels, num_samples))
        reordered_data[:, :] = current_data[indices, :]
        return reordered_data

def preprocess_eeg(eeg_data, recording_length=60):
    original_timepoints = eeg_data.shape[1]
    desired_timepoints = int(recording_length*60*100)
    frac = fractions.Fraction(desired_timepoints / original_timepoints).limit_denominator()
    resampled_data_approx = resample_poly(eeg_data, frac.numerator, frac.denominator, axis=1)
    eeg = resample(resampled_data_approx, desired_timepoints, axis=1)
    return eeg

def filter_physiological_boundaries(data, threshold=200):
    """Exclude values outside of physiological boundaries."""
    mask = np.abs(data) <= threshold  # Create a mask of values within the threshold
    data[~mask] = 0  # Set values outside of the threshold to zero
    return data

# Define Butterworth bandpass filter
def butter_bandpass(lowcut, highcut, fs, order=5):
    nyq = 0.5 * fs
    low = lowcut / nyq
    high = highcut / nyq
    b, a = butter(order, [low, high], btype='band')
    return b, a


def butter_bandpass_filter(data, lowcut, highcut, fs, order=5):
    b, a = butter_bandpass(lowcut, highcut, fs, order=order)
    if len(data.shape) == 1:  # If data is 1D, just apply the filter
        y = lfilter(b, a, data)
    elif len(data.shape) == 2:  # If data is 2D, apply the filter to each channel
        y = np.empty_like(data)  # Create an empty array with the same shape as data
        for channel in range(data.shape[0]):
            y[channel, :] = lfilter(b, a, data[channel, :])
    else:
        raise ValueError('Input data has an unsupported shape.')
    return y

def get_DC(data_folder, patient_id):
    # Load patient data.
    recording_ids = find_recording_files(data_folder, patient_id)
    num_recordings = len(recording_ids)
    # Extract ECG features.
    ecg_channels = ['ECG', 'ECGL', 'ECGR', 'ECG1', 'ECG2']
    group = 'ECG'

    if num_recordings > 0:
        current_ekg = []
        for recording_id in recording_ids:
            try:
                recording_location = os.path.join(data_folder, patient_id, '{}_{}'.format(recording_id, group))
                if os.path.exists(recording_location + '.hea'):
                    data, channels, sampling_rate = load_recording_data(recording_location)
                    data, channels = reduce_channels(data, channels, ecg_channels)
                    current_ekg.append(data)
            except Exception as Error:
                print(Error)
        if len(current_ekg) != 0:
            try:
                ekg = np.concatenate(current_ekg, axis=1)
            except:
                leads = try_recording_wise(data_folder, patient_id)
                return leads
            leads = {}
            for lead in range(len(ekg)):
                try:
                    current_lead = ekg[lead]
                    method = choose_ecg_method(current_lead, sampling_rate)
                    current_lead = nk.ecg_clean(current_lead, sampling_rate=sampling_rate)
                    _, rpeaks = nk.ecg_peaks(current_lead, method=method, sampling_rate=sampling_rate)
                    ecg_duration = int(len(current_lead) / sampling_rate)
                    print('Length ECG[h]', ecg_duration / 3600)
                    heart_rate = len(rpeaks['ECG_R_Peaks']) / ecg_duration * 60
                    print('Heart_rate_according_to_rpeaks:', heart_rate)
                    if heart_rate > 50:  # Calculate Inter Beat Interval
                        DC, BBDC, DCsgn, anchor_ibis = calculate_DC(rpeaks['ECG_R_Peaks'], sampling_rate=sampling_rate)
                        percentage_anchor_points = len(anchor_ibis) / (len(rpeaks['ECG_R_Peaks']) - 1) * 100
                        if 'percentage_anchor_points' not in leads or percentage_anchor_points > leads['percentage_anchor_points']:
                            leads['percentage_anchor_points'] = percentage_anchor_points
                            leads['DC'] = DC
                            leads['BBDC'] = BBDC
                            leads['DCsgn'] = DCsgn
                    else:
                        leads = try_recording_wise(data_folder, patient_id)
                        return leads
                except Exception as error:
                    print(error)
            if leads and percentage_anchor_points > 5:
                return leads
            else:
                leads = try_recording_wise(data_folder, patient_id)
                return leads
        else:
            leads = try_recording_wise(data_folder, patient_id)
            return leads
    return 0



def try_recording_wise(data_folder, patient_id):
    # Load patient data.
    recording_ids = find_recording_files(data_folder, patient_id)
    num_recordings = len(recording_ids)
    # Extract ECG features.
    ecg_channels = ['ECG', 'ECGL', 'ECGR', 'ECG1', 'ECG2']
    group = 'ECG'

    if num_recordings > 0:
        rr_intervals = {}
        for recording_id in recording_ids:
            recording_location = os.path.join(data_folder, patient_id,
                                              '{}_{}'.format(recording_id, group))
            try:
                if os.path.exists(recording_location + '.hea'):
                    data, channels, sampling_rate = load_recording_data(recording_location)
                    data, channels = reduce_channels(data, channels, ecg_channels)

                    for lead_num, lead_data in enumerate(data):
                        sampling_rate = int(sampling_rate)
                        if len(lead_data) > (
                                sampling_rate * 300):  # ensure that at least 5min ECG is present
                            try:
                                method = choose_ecg_method(lead_data, sampling_rate)
                                lead_data = nk.ecg_clean(lead_data, sampling_rate=sampling_rate)
                                _, rpeaks = nk.ecg_peaks(lead_data, method=method,
                                                         sampling_rate=sampling_rate)
                                ecg_duration = int(len(lead_data) / sampling_rate)
                                heart_rate = len(rpeaks['ECG_R_Peaks']) / ecg_duration * 60
                                # print('Heart_rate_according_to_rpeaks:', heart_rate)
                                if heart_rate > 50 and heart_rate < 180:  # Calculate Inter Beat Interval
                                    ibis = np.diff(rpeaks['ECG_R_Peaks'])
                                    ibis = ibis / sampling_rate * 1000  # Convert to ms
                                    if lead_num not in rr_intervals:
                                        rr_intervals[lead_num] = []
                                    rr_intervals[lead_num].extend(ibis)
                            except Exception as error:
                                print(error)
            except Exception as error:
                print(error)
        leads = {}
        for lead_num, intervals in rr_intervals.items():
            try:
                print(f"Lead Number: {lead_num}")
                print(f"RR Intervals: {len(intervals), intervals[:10]}")
                DC, BBDC, DCsgn, anchor_ibis = calculate_DC(intervals, sampling_rate=sampling_rate)
                percentage_anchor_points = len(anchor_ibis) / len(intervals) * 100
                if 'percentage_anchor_points' not in leads or percentage_anchor_points > leads[
                    'percentage_anchor_points']:
                    leads['percentage_anchor_points'] = percentage_anchor_points
                    leads['number_ibis'] = len(intervals)
                    leads['DC'] = DC
                    leads['BBDC'] = BBDC
                    leads['DCsgn'] = DCsgn
            except Exception as Error:
                print(Error)
        if leads:
            if leads['percentage_anchor_points'] > 5.0 or leads['number_ibis'] >= 80000:
                return leads
        return 0
    return 0


def calculate_DC(ibis, sampling_rate):
    try:
        anchor_ibis = []
        preceding_ibis = []
        pre_preceding_ibis = []
        postceding_ibis = []
        x_1 = []
        x_2 = []
        x_3 = []
        x_4 = []

        # Iterate through the ibis from the third element to the end
        for i in range(2, len(ibis) - 1):
            # Calculate the relative difference in percentage for anchor points
            absolute_difference_anchor = abs(ibis[i] - ibis[i - 1])
            relative_difference_anchor = (absolute_difference_anchor / ibis[i-1]) * 100
            absolute_difference_pre_preceding = abs(ibis[i - 1] - ibis[i - 2])
            relative_difference_pre_preceding = (absolute_difference_pre_preceding / ibis[i - 2]) * 100
            absolute_difference__postceding = abs(ibis[i + 1] - ibis[i])
            relative_difference_postceding = (absolute_difference__postceding / ibis[i]) * 100
            # Check conditions for the anchor point and the pre-preceding interval
            if (ibis[i] > ibis[i - 1] and relative_difference_anchor <= 5):
                anchor_ibis.append(ibis[i])
                preceding_ibis.append(ibis[i - 1])
                pre_preceding_ibis.append(ibis[i - 2])
                postceding_ibis.append(ibis[i + 1])
            if relative_difference_postceding <=5 and relative_difference_pre_preceding <=5 and relative_difference_anchor <=5:
                x_1.append(ibis[i - 2])
                x_2.append(ibis[i - 1])
                x_3.append(ibis[i])
                x_4.append(ibis[i + 1])
        dc_1 = []
        dc_2 = []
        dc_3 = []
        dc_4 = []
        for i in range(len(x_4)):
            acdc = (x_4[i] + x_3[i] - x_2[i] - x_1[i]) / 4
            if acdc > 0:
                dc_1.append(x_1[i])
                dc_2.append(x_2[i])
                dc_3.append(x_3[i])
                dc_4.append(x_4[i])
        percentage_DCsgn = (len(dc_2)/len(ibis))*100
        print('percentage_DCsgn', percentage_DCsgn)
        if percentage_DCsgn > 96.0 or percentage_DCsgn < 10.0:
            return 0,0,0,0

        # Calculate the averages
        average_anchor = sum(anchor_ibis) / len(anchor_ibis) if anchor_ibis else 0
        average_preceding = sum(preceding_ibis) / len(preceding_ibis) if preceding_ibis else 0
        average_pre_preceding = sum(pre_preceding_ibis) / len(pre_preceding_ibis) if pre_preceding_ibis else 0
        average_postceding = sum(postceding_ibis) / len(postceding_ibis) if postceding_ibis else 0
        average_dc_1 = sum(dc_1) / len(dc_1) if dc_1 else 0
        average_dc_2 = sum(dc_2) / len(dc_2) if dc_2 else 0
        average_dc_3 = sum(dc_3) / len(dc_3) if dc_3 else 0
        average_dc_4 = sum(dc_4) / len(dc_4) if dc_4 else 0
        if average_anchor != 0 and average_preceding != 0 and average_pre_preceding != 0 and average_postceding != 0:
            DC = (average_anchor + average_postceding - average_preceding - average_pre_preceding) / 4
            BBDC = (average_anchor-average_preceding)/2
            DCsgn = (average_dc_4 + average_dc_3 - average_dc_2 - average_dc_1) / 4
            return DC, BBDC, DCsgn, anchor_ibis
        else:
            return 0,0,0,0
    except Exception as error:
        print(error)
        return 0,0,0,0

def choose_ecg_method(ecg_signal, sampling_rate, base_sampling_rate=500, base_threshold=0.5, amplitude_threshold=50):
    """
    Detect spikes in ECG signal using its derivative and return the recommended R-peak detection method.

    Parameters:
    - ecg_signal: The ECG data.
    - sampling_rate: The sampling rate of the ECG data.
    - base_sampling_rate: The base sampling rate for which the threshold is defined (default is 500 Hz).
    - base_threshold: The base threshold for spike detection (default is 0.5).
    - amplitude_threshold: The minimum amplitude a spike should have to be considered valid (default is 50).

    Returns:
    - 'kalidas' if there are more downward spikes, 'neurokit' if there are more upward spikes.
    """
    # Scale the threshold
    threshold = base_threshold * (sampling_rate / base_sampling_rate)
    # Calculate the derivative
    derivative = np.diff(ecg_signal)
    # Find where the derivative crosses the threshold (both upward and downward)
    up_spikes = (derivative[:-1] > threshold) & (derivative[1:] < -threshold)
    down_spikes = (derivative[:-1] < -threshold) & (derivative[1:] > threshold)
    # Extract spike locations
    up_spike_locs = np.where(up_spikes)[0] + 1
    down_spike_locs = np.where(down_spikes)[0] + 1
    # Filter the spikes based on amplitude
    up_spike_locs = up_spike_locs[np.abs(ecg_signal[up_spike_locs]) > amplitude_threshold]
    down_spike_locs = down_spike_locs[np.abs(ecg_signal[down_spike_locs]) > amplitude_threshold]
    # Determine method
    if len(down_spike_locs) > len(up_spike_locs):
        return 'kalidas'
    else:
        return 'neurokit'

def get_categorical(data):
    sex = get_sex(data)
    sex_feature = np.zeros(3, dtype=int)
    if sex == 'Female':
        sex_feature[0] = 1
        sex_feature[2] = 1
    elif sex == 'Male':
        sex_feature[1] = 1
        sex_feature[2] = 1

    hospital = get_hospital(data)
    hospital_feature = np.zeros(6, dtype=int)
    if hospital == 'A':
        hospital_feature[0] = 1
    elif hospital == 'B':
        hospital_feature[1] = 1
    elif hospital == 'C':
        hospital_feature[2] = 1
    elif hospital == 'D':
        hospital_feature[3] = 1
    elif hospital == 'E':
        hospital_feature[4] = 1
    elif hospital == 'F':
        hospital_feature[5] = 1
    ohca = get_ohca(data)
    ohca_feature = np.zeros(3, dtype=int)
    if ohca == True:
        ohca_feature[0] = 1
        ohca_feature[2] = 1
    elif ohca == False:
        ohca_feature[1] = 1
        ohca_feature[2] = 1
    defi = get_shockable_rhythm(data)
    defi_feature = np.zeros(3, dtype=int)
    if defi == True:
        defi_feature[0] = 1
        defi_feature[2] = 1
    elif defi == False:
        defi_feature[1] = 1
        defi_feature[2] = 1
    ttm = get_ttm(data)
    ttm_feature = np.zeros(3, dtype=int)
    if ttm == 33:
        ttm_feature[0] = 1
        ttm_feature[2] = 1
    elif ttm == 36:
        ttm_feature[1] = 1
        ttm_feature[2] = 1
    return np.hstack([sex_feature, hospital_feature,ohca_feature, defi_feature, ttm_feature])
def get_numerical(data_folder, patient_id):
    patient_metadata = load_challenge_data(data_folder, patient_id)
    age = get_age(patient_metadata)
    age_feature = float(1-age/90)
    rosc = get_rosc(patient_metadata)
    # value near to 1 for low rosc times; float indicating whether the value is present
    rosc_feature = np.zeros(2, dtype=float)
    if type(rosc) == int:
        rosc_feature[0] = 1-rosc/100
        rosc_feature[1] = 1
    # value near to 1 for high DC; float indicating whether the value is present
    DC_feature = np.zeros(4, dtype=float)
    leads = try_recording_wise(data_folder, patient_id)

    if leads != 0:
        DC_feature[0] = 1
        if leads['DC'] > 0:
            DC_feature[1] = leads['DC'] / 10
        else:
            DC_feature[1] = 0
        DC_feature[2] = leads['BBDC'] / 10
        DC_feature[3] = leads['DCsgn'] / 10
    print(DC_feature)
    return np.hstack([age_feature, rosc_feature, DC_feature])


def fit_scaler_on_patients(data_folder, patient_ids):
    all_data = []
    for id in patient_ids:
        current_eeg = get_last_eeg(data_folder, id)
        if current_eeg.size > 0:  # Check for the size before appending
            all_data.append(current_eeg)
    all_data = np.concatenate(all_data, axis=1)  # concatenate along the second dimension
    scaler_bipolar = RobustScaler().fit(all_data.T)
    all_data = []
    for id in patient_ids:
        current_eeg = get_last_eeg(data_folder, id, raw=True)
        if current_eeg.size > 0:  # Check for the size before appending
            all_data.append(current_eeg)
    all_data = np.concatenate(all_data, axis=1)  # concatenate along the second dimension
    scaler_raw = RobustScaler().fit(all_data.T)
    del all_data
    gc.collect()
    return scaler_bipolar, scaler_raw

def calculate_bandpower(eeg_data, band, fs):
    # Ensure that eeg_data is long enough
    if len(eeg_data) < fs * 5:
        print(f"eeg_data must have at least {fs * 5} data points, but it has {len(eeg_data)}")
        return (0, 0)
    else:
        if band == (0.5, 4):
            nperseg = int(fs * 4)
        elif band in [(4, 8), (8, 12), (12, 30)]:
            nperseg = int(fs * 2)
        else:  # For gamma band
            nperseg = int(fs * 1)

        # Get the power spectral density.
        freqs, psd = welch(eeg_data, fs, nperseg=nperseg, noverlap=nperseg // 2, window='hann', scaling='density')
        freq_res = freqs[1] - freqs[0]
        idx_band = np.logical_and(freqs >= band[0], freqs < band[1])

        psd_band = simps(psd[idx_band], dx=freq_res)
        psd_total = simps(psd, dx=freq_res)
        if psd_total == 0:
            relative_power = 0
        else:
            relative_power = psd_band / psd_total
        psd_band = safe_replace_nan_with_zero(psd_band)
        relative_power = safe_replace_nan_with_zero(relative_power)
        return psd_band, relative_power


def calculate_all_bandpowers(eeg_data, fs=100):
    bands = [(0.5, 4.0), (4.0, 8.0), (8.0, 12.0), (12.0, 30.0), (30.0, 45.0)]
    expected_segments = int((3600*fs) / (fs*300))  # Assuming total time should be 60 minutes
    results = np.zeros((expected_segments, eeg_data.shape[0], 5, 2))

    num_windows = int((eeg_data.shape[1] - fs * 300) / (fs * 300)) + 1

    for i in range(num_windows):
        start = i * fs * 300
        end = start + fs * 300

        for j in range(eeg_data.shape[0]):
            for k, band in enumerate(bands):
                results[i, j, k, 0], results[i, j, k, 1] = calculate_bandpower(eeg_data[j, start:end], band, fs)

    return results

def create_bipolar_montage(data, channels=['Fp1', 'F7', 'T3', 'T5', 'O1', 'Fp2', 'F8', 'T4', 'T6', 'O2', 'F3', 'C3', 'P3', 'F4', 'C4', 'P4',
                    'Fz', 'Cz', 'Pz'], pairs=[('Fp1', 'F7'), ('F7', 'T3'), ('T3', 'T5'), ('T5', 'O1'), ('Fp2', 'F8'), ('F8', 'T4'), ('T4', 'T6'), ('T6', 'O2'), ('Fp1', 'F3'), ('F3', 'C3'), ('C3', 'P3'), ('P3', 'O1'), ('Fp2', 'F4'), ('F4', 'C4'), ('C4', 'P4'), ('P4', 'O2'), ('Fz', 'Cz'), ('Cz', 'Pz')]):
    '''
    data: raw eeg data with shape (channels, timepoints)
    channels: list of channels in the same order as they appear in the data
    pairs: list of pairs of channel names
    '''
    # Initialize a new array to store the bipolar data
    bipolar_data = np.zeros((len(pairs), data.shape[1]))

    # Subtract each pair of channels
    for pair_idx, pair in enumerate(pairs):
        ch1_idx = channels.index(pair[0])
        ch2_idx = channels.index(pair[1])
        bipolar_data[pair_idx, :] = data[ch1_idx, :] - data[ch2_idx, :]
    return bipolar_data

def calculate_coherence(eeg_data, fs=100, channels=['Fp1', 'F7', 'T3', 'T5', 'O1', 'Fp2', 'F8', 'T4', 'T6', 'O2', 'F3', 'C3', 'P3', 'F4', 'C4', 'P4',
                    'Fz', 'Cz', 'Pz'], pairs=[('Fp1', 'F7'), ('F7', 'T3'), ('T3', 'T5'), ('T5', 'O1'), ('Fp2', 'F8'), ('F8', 'T4'), ('T4', 'T6'), ('T6', 'O2'), ('Fp1', 'F3'), ('F3', 'C3'), ('C3', 'P3'), ('P3', 'O1'), ('Fp2', 'F4'), ('F4', 'C4'), ('C4', 'P4'), ('P4', 'O2'), ('Fz', 'Cz'), ('Cz', 'Pz')]):
    num_channels = eeg_data.shape[0]
    num_power_bands = 5

    power_bands = {'delta': (0.5, 4),
                   'theta': (4, 8),
                   'alpha': (8, 13),
                   'beta': (13, 30),
                   'gamma': (30, 45)}  # Adjust gamma frequency

    coherence_matrix = np.zeros((num_channels, num_channels, num_power_bands))

    pair_indices = [(channels.index(pair[0]), channels.index(pair[1])) for pair in pairs]
    pair_coherence_values = np.zeros((len(pair_indices), num_power_bands))

    for idx, (i, j) in enumerate(pair_indices):
        for k, (band_name, freq_range) in enumerate(power_bands.items()):  # Change here
            # Calculate coherence between channels i and j
            f, Cxy = coherence(eeg_data[i], eeg_data[j], fs=fs)

            # Select coherence values in the frequency band of interest
            indices = (f >= freq_range[0]) & (f <= freq_range[1])  # Use freq_range instead of band
            coh = Cxy[indices]


            coherence_matrix[i, j, k] = coh.mean()
            pair_coherence_values[idx, k] = coh.mean()
            coherence_matrix = safe_replace_nan_with_zero(coherence_matrix)
            pair_coherence_values = safe_replace_nan_with_zero(pair_coherence_values)

    return coherence_matrix, pair_coherence_values

def calculate_all_coherences(eeg_data,fs=100, channels=['Fp1', 'F7', 'T3', 'T5', 'O1', 'Fp2', 'F8', 'T4', 'T6', 'O2', 'F3', 'C3', 'P3', 'F4', 'C4', 'P4',
                    'Fz', 'Cz', 'Pz'], pairs=[('Fp1', 'F7'), ('F7', 'T3'), ('T3', 'T5'), ('T5', 'O1'), ('Fp2', 'F8'), ('F8', 'T4'), ('T4', 'T6'), ('T6', 'O2'), ('Fp1', 'F3'), ('F3', 'C3'), ('C3', 'P3'), ('P3', 'O1'), ('Fp2', 'F4'), ('F4', 'C4'), ('C4', 'P4'), ('P4', 'O2'), ('Fz', 'Cz'), ('Cz', 'Pz')], window_sec=300, step_sec=300):
    num_bands = 5
    expected_windows = 12
    num_samples = eeg_data.shape[1]

    pair_coherence_values_all = np.zeros((expected_windows, len(pairs), num_bands))
    old_settings = np.seterr(divide='ignore', invalid='ignore')
    for i in range(expected_windows):
        start = i * fs * step_sec
        end = start + fs * window_sec

        if end > num_samples:
            temp_eeg = np.pad(eeg_data[:, start:], ((0, 0), (0, end - num_samples)), 'constant', constant_values=0)
        else:
            temp_eeg = eeg_data[:, start:end]

        _, pair_coherence_values = calculate_coherence(temp_eeg, fs, channels, pairs)
        pair_coherence_values_all[i] = pair_coherence_values
    np.seterr(**old_settings)
    return pair_coherence_values_all


def pa_series(x_phase, x_amp):
    phase = np.angle(hilbert(x_phase))
    amplitude = np.abs(hilbert(x_amp))
    pac = np.abs(np.mean(amplitude * np.exp(1j * phase)))
    pac = safe_replace_nan_with_zero(pac)
    return pac


# Define function to calculate PAC for each window of data
def calculate_window_pac(eeg_data, fs=100, window_sec=300, step_sec=300,
                         channels=['Fp1', 'F7', 'T3', 'T5', 'O1', 'Fp2', 'F8', 'T4', 'T6', 'O2', 'F3', 'C3', 'P3', 'F4',
                                   'C4', 'P4',
                                   'Fz', 'Cz', 'Pz'],
                         pairs=[('Fp1', 'F7'), ('F7', 'T3'), ('T3', 'T5'), ('T5', 'O1'), ('Fp2', 'F8'), ('F8', 'T4'),
                                ('T4', 'T6'), ('T6', 'O2'), ('Fp1', 'F3'), ('F3', 'C3'), ('C3', 'P3'), ('P3', 'O1'),
                                ('Fp2', 'F4'), ('F4', 'C4'), ('C4', 'P4'), ('P4', 'O2'), ('Fz', 'Cz'), ('Cz', 'Pz')]):
    freq_bands = [{'name': 'delta-theta', 'low': (0.5, 4), 'high': (4, 8)},
                  {'name': 'theta-alpha', 'low': (4, 8), 'high': (8, 12)},
                  {'name': 'alpha-beta', 'low': (8, 12), 'high': (12, 30)},
                  {'name': 'beta-gamma', 'low': (12, 30), 'high': (30, 45)},
                  {'name': 'alpha-gamma', 'low': (8, 12), 'high': (30, 45)}]

    num_windows = 12  # Set number of windows to 12
    pair_indices = [(channels.index(pair[0]), channels.index(pair[1])) for pair in pairs]
    pac_values_all = np.zeros((num_windows, len(pair_indices), len(freq_bands)))  # Initialize all values to zero

    num_windows_data = int((eeg_data.shape[1] - fs * window_sec) / (fs * step_sec)) + 1  # Calculate number of windows in the data

    for w in range(num_windows_data):
        start = w * fs * step_sec
        end = start + fs * window_sec
        window_data = eeg_data[:, start:end]

        for idx, (i, j) in enumerate(pair_indices):
            for k, band in enumerate(freq_bands):
                low_freq_phase_providing_signal = butter_bandpass_filter(window_data[i, :], band['low'][0],
                                                                         band['low'][1], fs)
                high_freq_amplitude_providing_signal = butter_bandpass_filter(window_data[j, :], band['high'][0],
                                                                              band['high'][1], fs)
                pac = pa_series(low_freq_phase_providing_signal, high_freq_amplitude_providing_signal)
                pac_values_all[w, idx, k] = pac

    return pac_values_all


class DataGenerator(Sequence):
    def __init__(self, X_categorical, X_numerical, X_timeseries, y, batch_size=8):
        self.X_categorical = X_categorical
        self.X_numerical = X_numerical
        self.X_timeseries = X_timeseries
        self.y = y
        self.batch_size = batch_size
        self.indexes = np.arange(len(self.y))

    def __len__(self):
        return int(np.ceil(len(self.y) / self.batch_size))

    def __getitem__(self, index):
        # Get batch indexes
        start_idx = index * self.batch_size
        end_idx = (index + 1) * self.batch_size

        batch_indexes = self.indexes[start_idx:end_idx]

        X_cat_batch = np.array([self.X_categorical[i] for i in batch_indexes])
        X_num_batch = np.array([self.X_numerical[i] for i in batch_indexes])
        X_time_batch = np.array([self.X_timeseries[i] for i in batch_indexes])
        y_batch = np.array([self.y[i] for i in batch_indexes])

        # Create a dictionary of inputs for the model
        X_batch = {
            "input_categorical": X_cat_batch,
            "input_numerical": X_num_batch,
            "input_timeseries": X_time_batch
        }

        return X_batch, y_batch

    def on_epoch_end(self):
        # Shuffle the data after each epoch
        np.random.shuffle(self.indexes)


class create_model():
    def __init__(self, input_shape_categorical, input_shape_numerical, input_shape_timeseries, output_size):
        self.input_shape_categorical = input_shape_categorical
        self.input_shape_numerical = input_shape_numerical
        self.input_shape_timeseries = input_shape_timeseries
        self.output_size = output_size  # True for binary classification, False for multi-class

    def build(self):
        input_categorical = layers.Input(shape=self.input_shape_categorical, name="input_categorical")
        input_numerical = layers.Input(shape=self.input_shape_numerical, name="input_numerical")
        input_timeseries = layers.Input(shape=self.input_shape_timeseries, name="input_timeseries")

        dense_categorical = layers.Dense(units=16, activation='relu')(input_categorical)
        dense_numerical = layers.Dense(units=48, activation='relu')(input_numerical)

        masked_timeseries = layers.Masking(mask_value=0.0)(input_timeseries)

        lstm_out_first_12h = layers.LSTM(units=16, return_sequences=False)(masked_timeseries)
        lstm_out_first_24h = layers.LSTM(units=64, return_sequences=False)(masked_timeseries)
        lstm_out_upto_48h = layers.LSTM(units=64, return_sequences=False)(masked_timeseries)
        lstm_out_upto_72h = layers.LSTM(units=112, return_sequences=False)(masked_timeseries)

        lstm_out_first_12h = layers.Dense(units=16, activation='relu')(lstm_out_first_12h)
        lstm_out_first_24h = layers.Dense(units=16, activation='relu')(lstm_out_first_24h)
        lstm_out_upto_48h = layers.Dense(units=40, activation='relu')(lstm_out_upto_48h)
        lstm_out_upto_72h = layers.Dense(units=32, activation='relu')(lstm_out_upto_72h)

        lstm_concat = layers.concatenate([lstm_out_first_12h, lstm_out_first_24h, lstm_out_upto_48h, lstm_out_upto_72h])

        concat = layers.concatenate([dense_categorical, dense_numerical, lstm_concat])

        concat = layers.BatchNormalization()(concat)

        dropout = layers.Dropout(rate=0.2)(concat)

        if self.output_size:
            activation = 'sigmoid'
            loss_fn = keras.losses.BinaryCrossentropy()
            metrics = [keras.metrics.AUC(name='auc')]
            output_units = 1
        else:
            activation = 'softmax'
            loss_fn = keras.losses.CategoricalCrossentropy()
            metrics = [keras.metrics.AUC(name='auc')]
            output_units = 5

        output = layers.Dense(output_units, activation=activation)(dropout)

        model = keras.Model(inputs=[input_categorical, input_numerical, input_timeseries], outputs=output)

        model.compile(
            optimizer=keras.optimizers.Adam(learning_rate=0.001),
            loss=loss_fn,
            metrics=metrics
        )

        return model
